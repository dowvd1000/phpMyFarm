CREATE DATABASE IF NOT EXISTS `farm`;
USE `farm`;

CREATE TABLE IF NOT EXISTS `data` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '0',
  `pass` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `settings` (`id`, `name`, `address`, `phone`) VALUES
(1, 'Farm Name', 'Address', 'Phone Number');

CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tare` int(10) NOT NULL,
  `gross` int(10) NOT NULL,
  `moisture` int(10) NOT NULL,
  `netkg` int(11) NOT NULL,
  `netmt` decimal(10,2) NOT NULL,
  `crop` varchar(15) NOT NULL,
  `farm` varchar(100) NOT NULL,
  `truck` varchar(100) NOT NULL,
  `farmer` varchar(100) NOT NULL,
  `trucker` varchar(100) NOT NULL,
  `dockage` decimal(10,1) NOT NULL,
  `ccfm` decimal(10,1) NOT NULL,
  `test` varchar(100) NOT NULL,
  `grade` int(10) NOT NULL,
  `handeling` decimal(10,2) NOT NULL,
  `drying` decimal(10,2) NOT NULL,
  `notes` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

ALTER TABLE `data`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
ALTER TABLE `login`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
ALTER TABLE `settings`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE `tickets`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

