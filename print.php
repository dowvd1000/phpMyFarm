<?php
session_start();
$session = $_SESSION['login'];
if (!(isset($session) && $session != '')) {
  header ("Location: login.php?lastloc=".$_SERVER['REQUEST_URI']);
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	  <link rel="stylesheet" href="lib/bootstrap/dist/css/bootstrap.min.css" type="text/css" />
	  <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css" type="text/css" />
	  <link rel="stylesheet" href="dist/css/styles.min.css" type="text/css" />
    <style>
      hr {
        border-color: #000000;
      }
    </style>
	  <link rel="icon" href="assets/icons/favicon.png">
		<title>Ticket Printed by phpMyFarm</title>
	</head>
<body>
	<div class="noprint">
		<button data-action="print" class="btn btn-default">Print</button>
	</div>
	<?php
	require_once 'config/dbconn.php';

	$query = "SELECT * FROM `settings`";
	$stmt = $DBcon->prepare($query);
	$stmt->execute();
	while ($row=$stmt->fetch(PDO::FETCH_ASSOC) ) {
		extract($row);
		$grainname = $name;
		$address = $address;
		$phone = $phone;
	}

	$print = $_GET['print'];
	$print = trim($_GET['print']);
	$print = strip_tags($print);
	$print = htmlspecialchars($print);
	if (isset($print)) {
		$query = "SELECT * FROM `tickets` WHERE (`id` LIKE '%".$print."%')";
		$stmt = $DBcon->prepare($query);
		$stmt->execute();
		while ($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
			extract($row);
	?>
	<div class="title">
	  <h3><?php echo $grainname; ?></h3>
	  <span><?php echo $address; ?>&nbsp;|&nbsp;<?php echo $phone; ?></span>
	</div>
	<hr/>
	<ul class="left">
	  <li><?php echo $farmer; ?></li>
	</ul>
	<ul class="right">
	  <li>Ticket #<?php echo $id; ?></li>
	  <li><?php echo $crop; ?></li>
	</ul>
	<div class="table">
		<div class="date">
			<h4><?php echo $time; ?></h4>
		</div>
    <hr/>
		<table>
			<tr>
				<td class="one">Gross:&nbsp;<?php echo $gross; ?>&nbsp;kg</td>
				<td class="two">Moisture:&nbsp;<?php echo $moisture; ?>%</td>
				<td class="three">Grade:&nbsp;<?php echo $grade; ?></td>
			</tr>
			<tr>
				<td class="one">Tare:&nbsp;<?php echo $tare; ?>&nbsp;kg</td>
				<td class="two">Dockage:&nbsp;<?php echo $dockage; ?>%</td>
				<td class="three">Farm:&nbsp;<?php echo $farm; ?></td>
			</tr>
			<tr>
				<td class="one">Net:&nbsp;<?php echo $netkg; ?>&nbsp;kg</td>
				<td class="two">CCFM:&nbsp;<?php echo $ccfm; ?>%</td>
				<td class="three">Trucker:&nbsp;<?php echo $trucker; ?></td>
			</tr>
			<tr>
				<td class="one">Dry:&nbsp;<?php echo $netmt; ?>&nbsp;mt</td>
				<td class="two">Test Weight:&nbsp;<?php echo $test; ?></td>
				<td class="three">Truck:&nbsp;<?php echo $truck; ?></td>
			</tr>
		</table>
	</div>

	<br/>
	<div class="cut"></div>
	<br/>

	<div class="title">
	  <h3><?php echo $grainname; ?></h3>
	  <span><?php echo $address; ?>&nbsp;|&nbsp;<?php echo $phone; ?></span>
	</div>
	<hr/>
	<ul class="left">
	  <li><?php echo $farmer; ?></li>
	</ul>
	<ul class="right">
	  <li>Ticket #<?php echo $id; ?></li>
	  <li>Crop&nbsp;<?php echo $crop; ?></li>
	</ul>
	<div class="table">
		<div class="date">
			<h4><?php echo $time; ?></h4>
		</div>
    <hr/>
		<table>
			<tr>
				<td class="one">Gross:&nbsp;<?php echo $gross; ?>&nbsp;kg</td>
				<td class="two">Moisture:&nbsp;<?php echo $moisture; ?>%</td>
				<td class="three">Grade:&nbsp;<?php echo $grade; ?></td>
			</tr>
			<tr>
				<td class="one">Tare:&nbsp;<?php echo $tare; ?>&nbsp;kg</td>
				<td class="two">Dockage:&nbsp;<?php echo $dockage; ?>%</td>
				<td class="three">Farm:&nbsp;<?php echo $farm; ?></td>
			</tr>
			<tr>
				<td class="one">Net:&nbsp;<?php echo $netkg; ?>&nbsp;kg</td>
				<td class="two">CCFM:&nbsp;<?php echo $ccfm; ?>%</td>
				<td class="three">Trucker:&nbsp;<?php echo $trucker; ?></td>
			</tr>
			<tr>
				<td class="one">Dry:&nbsp;<?php echo $netmt; ?>&nbsp;mt</td>
				<td class="two">Test Weight:&nbsp;<?php echo $test; ?></td>
				<td class="three">Truck:&nbsp;<?php echo $truck; ?></td>
			</tr>
		</table>
	</div>

	<?php
	}}
	?>
	<script src="lib/jquery/dist/jquery.min.js"></script>
  <script src="lib/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="lib/bootbox.js/bootbox.js"></script>
  <script src="dist/js/all.min.js"></script>
</body>
<html>
