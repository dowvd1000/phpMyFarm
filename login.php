<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <link rel="stylesheet" href="lib/bootstrap/dist/css/bootstrap.min.css" type="text/css" />
  <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="dist/css/login.min.css" type="text/css" />
  <link rel="stylesheet" href="dist/css/styles.min.css" type="text/css" />
  <link rel="icon" href="assets/icons/favicon.png">
  <title>Login phpMyFarm</title>
</head>
<body>
  <div class = "container">
    <div class="wrapper">
      <?php
      session_start();

      include_once 'config/dbconn.php';
      require_once 'config/userConfig.php';

      $name0 = $_POST['name'];
      $name0 = trim($_POST['name']);
      $name0 = strip_tags($name0);
      $name0 = htmlspecialchars($name0);

      $pass0 = $_POST['pass'];
      $pass0 = trim($_POST['pass']);
      $pass0 = strip_tags($pass0);
      $pass0 = htmlspecialchars($pass0);

      $hashp = hash('sha512', $pass0 . $salt);

      if (isset($_POST['go'])) {
        if ($name0 != '' || $pass0 != '') {
          if ($enableSetupCreds == true) {
            if ($name0 == $setupCreds[0] && $pass0 == $setupCreds[1]) {
              $_SESSION["login"] = $name0;
              header("Location: /");
            } else {
              echo "
              <div class='alert alert-danger fade in'>
                <i class='fa fa-times' aria-hidden='true'></i>&nbsp;<strong>Error:</strong>&nbsp;Username/Password incorect.
              </div>";
            }
          } else {
            $query = "SELECT `name`, `pass` FROM `login` WHERE `name`='$name0' AND `pass`='$hashp'";
            $stmt = $DBcon->prepare($query);
            $stmt->execute();
            $count = $stmt->rowCount();
            if ($count == 1) {
              $_SESSION["login"] = $name0;
              header("Location: index.php");
            } else {
              echo "
              <div class='alert alert-danger fade in'>
                <i class='fa fa-times' aria-hidden='true'></i>&nbsp;<strong>Error:</strong>&nbsp;Username/Password incorect.
              </div>";
            }
          }
        }
      }

      if (isset($_GET['logout'])) {
        session_unset();
        session_destroy();
      }
      ?>
		  <form action="login.php" method="POST" name="login" class="form-signin">
		    <h4 class="form-signin-heading">Sign In To phpMyFarm</h4>
			  <input type="text" class="form-control" name="name" placeholder="Username" required autofocus />
			  <input type="password" class="form-control" name="pass" placeholder="Password" required />
			  <button class="btn btn-primary btn-block" name="go" value="Login" type="submit">Login</button>
		  </form>
	  </div>
  </div>
  <footer class="footer">
    <div class="container">
      <hr/>
      <p class="text-muted">&copy; 2016 - 2017 Dow Van Dine</p>
    </div>
  </footer>
  <script src="lib/jquery/dist/jquery.min.js"></script>
  <script src="lib/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="lib/bootbox.js/bootbox.js"></script>
  <script src="dist/js/all.min.js"></script>
</body>
<html/>
