<?php
require_once 'config/dbconn.php';

$pid = $_POST['pid0'];

$tare = $_POST['tare0'];
$tare = trim($_POST['tare0']);
$tare = strip_tags($tare);
$tare = htmlspecialchars($tare);

$gross = $_POST['gross0'];
$gross = trim($_POST['gross0']);
$gross = strip_tags($gross);
$gross = htmlspecialchars($gross);

$moisture = $_POST['moisture0'];
$moisture = trim($_POST['moisture0']);
$moisture = strip_tags($moisture);
$moisture = htmlspecialchars($moisture);

$crop = $_POST['crop0'];
$crop = trim($_POST['crop0']);
$crop = strip_tags($crop);
$crop = htmlspecialchars($crop);

$farm = $_POST['farm0'];
$farm = trim($_POST['farm0']);
$farm = strip_tags($farm);
$farm = htmlspecialchars($farm);

$truck = $_POST['truck0'];
$truck = trim($_POST['truck0']);
$truck = strip_tags($truck);
$truck = htmlspecialchars($truck);

$dockage = $_POST['dockage0'];
$dockage = trim($_POST['dockage0']);
$dockage = strip_tags($dockage);
$dockage = htmlspecialchars($dockage);

$ccfm = $_POST['ccfm0'];
$ccfm = trim($_POST['ccfm0']);
$ccfm = strip_tags($ccfm);
$ccfm = htmlspecialchars($ccfm);

$farmer = $_POST['farmer0'];
$farmer = trim($_POST['farmer0']);
$farmer = strip_tags($farmer);
$farmer = htmlspecialchars($farmer);

$trucker = $_POST['trucker0'];
$trucker = trim($_POST['trucker0']);
$trucker = strip_tags($trucker);
$trucker = htmlspecialchars($trucker);

$test = $_POST['test0'];
$test = trim($_POST['test0']);
$test = strip_tags($test);
$test = htmlspecialchars($test);

$grade = $_POST['grade0'];
$grade = trim($_POST['grade0']);
$grade = strip_tags($grade);
$grade = htmlspecialchars($grade);

$net0 = $gross - $tare;

$dockage0 = ($dockage / 100) * $net0;
$ccfm0 = ($ccfm / 100) * $net0;

$netkg = $net0 - $dockage0 - $ccfm0;
//$netmt = $netkg * 0.001;
$handeling = $netkg * 0.0038;

$net = $netkg; //name change

if ($crop == "canola"){
	require "assets/tables/can.php";
}
if ($crop == "corn"){
	require "assets/tables/cor.php";
}
if ($crop == "soybean") {
	require "assets/tables/soy.php";
}
if ($crop == "wheat"){
	require "assets/tables/whe.php";
}

$drying = round($d,2);
$netmt = round($mt,2);

$query = "UPDATE
  `tickets`
SET
  `tare`='$tare',
  `gross`='$gross',
	`netkg`='$netkg',
	`netmt`='$netmt',
  `moisture`='$moisture',
  `crop`='$crop',
  `farm`='$farm',
  `truck`='$truck',
  `farmer`='$farmer',
  `trucker`='$trucker',
  `dockage`='$dockage',
  `ccfm`='$ccfm',
  `test`='$test',
	`drying`='$drying',
	`handeling`='$handeling',
  `grade`='$grade'
WHERE
 `id`='$pid'";

$stmt = $DBcon->prepare($query);
$stmt->execute();
?>
