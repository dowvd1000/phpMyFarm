<?php
require_once 'config/dbconn.php';
if ($_REQUEST['delete']) {
  $pid = $_REQUEST['delete'];
  $query = "DELETE FROM `tickets` WHERE id=:pid";
  $stmt = $DBcon->prepare($query);
  $stmt->execute(array(':pid'=>$pid));
  if ($stmt) {
    echo "Failed to remove ticket from database.";
  }
}
?>
