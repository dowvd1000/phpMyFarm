<?php
include_once 'userConfig.php';

try {
	$DBcon = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
	$DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $error) {
	die($error->getMessage());
}
?>
