<?php
session_start();
$session = $_SESSION['login'];
if (!(isset($session) && $session != '')) {
  header ("Location: login.php?lastloc=".$_SERVER['REQUEST_URI']);
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	  <link rel="stylesheet" href="lib/bootstrap/dist/css/bootstrap.min.css" type="text/css" />
	  <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css" type="text/css" />
	  <link rel="stylesheet" href="dist/css/styles.min.css" type="text/css" />
    <style>
      hr {
        border-color: #000000;
      }
    </style>
	  <link rel="icon" href="assets/icons/favicon.png">
		<title>Report Printed by phpMyFarm</title>
	</head>
<body>
	<div class="noprint">
		<button data-action="print" class="btn btn-default">Print</button>
	</div>
	<?php
	require_once 'config/dbconn.php';

  $query = "SELECT * FROM `settings`";
	$stmt = $DBcon->prepare($query);
	$stmt->execute();
	while ($row=$stmt->fetch(PDO::FETCH_ASSOC) ) {
		extract($row);
		$grainname = $name;
		$address = $address;
		$phone = $phone;
	}

	$report = $_GET['total'];
	$report = trim($_GET['total']);
	$report = strip_tags($report);
	$report = htmlspecialchars($report);
	if (isset($report)) {
		$query = "SELECT `farmer`, `farm`, `crop`,
							SUM(`gross`) AS `tgross`,
							SUM(`tare`) AS `ttare`,
							SUM(`netkg`) AS `tnetkg`,
							SUM(`netmt`) AS `tnetmt`,
							SUM(`drying`) AS `tdrying`,
							SUM(`handeling`) AS `thandeling`,
							AVG(`moisture`) AS `amos`
							FROM `tickets`
							WHERE (`farm` LIKE '%".$report."%')";
		$stmt = $DBcon->prepare($query);
		$stmt->execute();
		$rno = $stmt->rowCount();
		while ($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
		  extract($row);
			?>
			<div class="title">
				<h3><?php echo $grainname; ?></h3>
				<span><?php echo $address; ?>&nbsp;|&nbsp;<?php echo $phone; ?></span>
			</div>
			<hr/>
			<ul class="left">
				<li><?php echo $farmer; ?></li>
			</ul>
			<ul class="right">
				<li><?php echo $farm; ?></li>
				<li><?php echo $crop?></li>
			</ul>
			<div class="table">
				<div class="date">
					<h4><?php
					echo date('Y-m-d H:i:s');
					?></h4>
				</div>
        <hr/>
				<table>
					<tr>
						<td class="one">Total Gross:&nbsp;<?php echo $tgross; ?>&nbsp;kg</td>
					</tr>
					<tr>
						<td class="one">Total Tare:&nbsp;<?php echo $ttare; ?>&nbsp;kg</td>
					</tr>
					<tr>
						<td class="one">Total Net:&nbsp;<?php echo $tnetkg; ?>&nbsp;kg</td>
					</tr>
					<tr>
						<?php
						 	$mt0 = $net * 0.001;
							$cropyr = date('Y') - 1;
						?>
						<td class="one">Total Dry:&nbsp;<?php echo $tnetmt; ?>&nbsp;mt</td>
					</tr>
					<tr>
						<td class="one">AVG Moisture:&nbsp;<?php echo round($amos,1); ?>%</td>
					</tr>
          <tr>
						<td class="one">Total Handling Charge:&nbsp;$<?php echo $thandeling; ?></td>
					</tr>
					<tr>
						<td class="one">Total Drying Charge:&nbsp;$<?php echo $tdrying; ?></td>
					</tr>
					<tr>
						<td class="one">Crop Year:&nbsp;<?php echo $cropyr; ?></td>
					</tr>
				</table>
			</div>
			<br/>
			<div class="cut"></div>
			<br/>
	<?php }} ?>
	<script src="lib/jquery/dist/jquery.min.js"></script>
  <script src="lib/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="lib/bootbox.js/bootbox.js"></script>
  <script src="dist/js/all.min.js"></script>
</body>
<html>
