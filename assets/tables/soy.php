<?php
require_once 'moisture.funtion.php';
/*-----------------------------------------------------------------------------
Drying Table for Soybeans
-----------------------------------------------------------------------------*/
#________Moisture___Net___Mos___Ton_____Dry
Moisture($moisture, $net, 12.9, 1000.0, 0.0);
Moisture($moisture, $net, 13.0, 1000.0, 0.0);
Moisture($moisture, $net, 13.1, 1006.2, 0.0);
Moisture($moisture, $net, 13.2, 1007.3, 0.0);
Moisture($moisture, $net, 13.3, 1008.5, 0.0);
Moisture($moisture, $net, 13.4, 1009.6, 0.0);
Moisture($moisture, $net, 13.5, 1010.8, 0.0);
Moisture($moisture, $net, 13.6, 1011.9, 0.0);
Moisture($moisture, $net, 13.7, 1013.1, 0.0);
Moisture($moisture, $net, 13.8, 1014.3, 0.0);
Moisture($moisture, $net, 13.9, 1015.6, 0.0);
Moisture($moisture, $net, 14.0, 1021.7, 0.0);
Moisture($moisture, $net, 14.1, 1022.9, 1.4);
Moisture($moisture, $net, 14.2, 1024.1, 1.8);
Moisture($moisture, $net, 14.3, 1025.3, 2.2);
Moisture($moisture, $net, 14.4, 1026.6, 2.6);
Moisture($moisture, $net, 14.5, 1027.7, 3.0);
Moisture($moisture, $net, 14.6, 1028.9, 3.5);
Moisture($moisture, $net, 14.7, 1030.1, 4.0);
Moisture($moisture, $net, 14.8, 1031.3, 4.5);
Moisture($moisture, $net, 14.9, 1032.5, 5.0);
Moisture($moisture, $net, 15.0, 1034.8, 5.5);
Moisture($moisture, $net, 15.1, 1036.0, 6.0);
Moisture($moisture, $net, 15.2, 1037.2, 6.5);
Moisture($moisture, $net, 15.3, 1038.5, 7.0);
Moisture($moisture, $net, 15.4, 1039.7, 7.5);
Moisture($moisture, $net, 15.5, 1040.9, 8.0);
Moisture($moisture, $net, 15.6, 1042.1, 8.4);
Moisture($moisture, $net, 15.7, 1043.4, 8.9);
Moisture($moisture, $net, 15.8, 1044.7, 9.2);
Moisture($moisture, $net, 15.9, 1045.9, 9.7);
Moisture($moisture, $net, 16.0, 1048.1, 10.3);
Moisture($moisture, $net, 16.1, 1049.3, 10.7);
Moisture($moisture, $net, 16.2, 1050.6, 11.2);
Moisture($moisture, $net, 16.3, 1051.9, 11.6);
Moisture($moisture, $net, 16.4, 1053.2, 12.1);
Moisture($moisture, $net, 16.5, 1054.4, 12.4);
Moisture($moisture, $net, 16.6, 1055.7, 12.7);
Moisture($moisture, $net, 16.7, 1056.9, 13.0);
Moisture($moisture, $net, 16.8, 1058.2, 13.3);
Moisture($moisture, $net, 16.9, 1059.5, 13.6);
Moisture($moisture, $net, 17.0, 1061.8, 13.9);
Moisture($moisture, $net, 17.1, 1063.1, 14.2);
Moisture($moisture, $net, 17.2, 1064.4, 14.5);
Moisture($moisture, $net, 17.3, 1065.7, 14.8);
Moisture($moisture, $net, 17.4, 1067.0, 15.1);
Moisture($moisture, $net, 17,5, 1068.2, 15.4);
Moisture($moisture, $net, 17.6, 1069.5, 15.7);
Moisture($moisture, $net, 17.7, 1070.8, 16.0);
Moisture($moisture, $net, 17.8, 1072.2, 16.3);
Moisture($moisture, $net, 17.9, 1073.5, 16.6);
Moisture($moisture, $net, 18.0, 1075.9, 16.9);
Moisture($moisture, $net, 18.1, 1077.2, 17.2);
Moisture($moisture, $net, 18.2, 1078.5, 17.5);
Moisture($moisture, $net, 18.3, 1079.8, 17.8);
Moisture($moisture, $net, 18.4, 1081.1, 18.1);
Moisture($moisture, $net, 18.5, 1082.4, 18.5);
Moisture($moisture, $net, 18.6, 1083.8, 18.8);
Moisture($moisture, $net, 18.7, 1085.1, 19.1);
Moisture($moisture, $net, 18.8, 1086.4, 19.4);
Moisture($moisture, $net, 18.9, 1087.7, 19.7);
Moisture($moisture, $net, 19.0, 1090.2, 20.0);
Moisture($moisture, $net, 19.1, 1091.5, 20.3);
Moisture($moisture, $net, 19.2, 1092.9, 20.6);
Moisture($moisture, $net, 19.3, 1094.3, 20.9);
Moisture($moisture, $net, 19.4, 1095.6, 21.2);
Moisture($moisture, $net, 19.5, 1096.9, 21.5);
Moisture($moisture, $net, 19.6, 1098.3, 21.8);
Moisture($moisture, $net, 19.7, 1099.7, 22.1);
Moisture($moisture, $net, 19.8, 1101.1, 22.4);
Moisture($moisture, $net, 19.9, 1102.4, 22.7);
Moisture($moisture, $net, 20.0, 1104.9, 23.0);
Moisture($moisture, $net, 20.1, 1106.2, 23.4);
Moisture($moisture, $net, 20.2, 1107.6, 23.7);
Moisture($moisture, $net, 20.3, 1109.1, 24.0);
Moisture($moisture, $net, 20.4, 1110.5, 24.3);
Moisture($moisture, $net, 20.5, 1111.9, 24.6);
Moisture($moisture, $net, 20.6, 1113.3, 25.0);
Moisture($moisture, $net, 20.7, 1114.7, 25.3);
Moisture($moisture, $net, 20.8, 1116.1, 25.6);
Moisture($moisture, $net, 20.9, 1117.5, 25.9);
Moisture($moisture, $net, 21.0, 1120.0, 26.2);
Moisture($moisture, $net, 21.1, 1121.4, 26.5);
Moisture($moisture, $net, 21.2, 1122.9, 26.8);
Moisture($moisture, $net, 21.3, 1124.3, 27.1);
Moisture($moisture, $net, 21.4, 1125.7, 27.5);
Moisture($moisture, $net, 21.5, 1127.1, 27.8);
Moisture($moisture, $net, 21.6, 1128.7, 28.1);
Moisture($moisture, $net, 21.7, 1130.0, 28.4);
Moisture($moisture, $net, 21.8, 1131.4, 28.8);
Moisture($moisture, $net, 21.9, 1132.9, 29.1);
Moisture($moisture, $net, 22.0, 1135.3, 29.4);
Moisture($moisture, $net, 22.1, 1136.9, 29.7);
Moisture($moisture, $net, 22.2, 1138.3, 30.0);
Moisture($moisture, $net, 22.3, 1139.8, 29.8);
Moisture($moisture, $net, 22.4, 1141.2, 30.0);
Moisture($moisture, $net, 22.5, 1142.8, 30.3);
Moisture($moisture, $net, 22.6, 1144.2, 30.7);
Moisture($moisture, $net, 22.7, 1145.7, 31.0);
Moisture($moisture, $net, 22.8, 1147.1, 31.3);
Moisture($moisture, $net, 22.9, 1148.7, 31.6);
Moisture($moisture, $net, 23.0, 1151.2, 31.9);
Moisture($moisture, $net, 23.1, 1152.7, 32.2);
Moisture($moisture, $net, 23.2, 1154.3, 32.5);
Moisture($moisture, $net, 23.3, 1155.8, 32.8);
Moisture($moisture, $net, 23.4, 1157.2, 33.1);
Moisture($moisture, $net, 23.5, 1158.9, 33.4);
Moisture($moisture, $net, 23.6, 1160.3, 33.7);
Moisture($moisture, $net, 23.7, 1161.9, 34.0);
Moisture($moisture, $net, 23.8, 1163.3, 34.4);
Moisture($moisture, $net, 23.9, 1164.9, 34.8);
Moisture($moisture, $net, 24.0, 1167.5, 35.5);
Moisture($moisture, $net, 24.1, 1169.2, 35.6);
Moisture($moisture, $net, 24.2, 1170.6, 36.2);
Moisture($moisture, $net, 24.3, 1172.2, 36.5);
Moisture($moisture, $net, 24.4, 1173.7, 37.0);
Moisture($moisture, $net, 24.5, 1175.3, 37.5);
Moisture($moisture, $net, 24.6, 1176.8, 38.0);
Moisture($moisture, $net, 24.7, 1178.5, 38.5);
Moisture($moisture, $net, 24.8, 1180.0, 39.0);
Moisture($moisture, $net, 24.9, 1181.6, 39.5);
Moisture($moisture, $net, 25.0, 1183.2, 40.0);
//End
?>
