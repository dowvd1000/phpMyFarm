<?php
session_start();
$session = $_SESSION['login'];
if (!(isset($session) && $session != '')) {
  header ("Location: login.php?lastloc=".$_SERVER['REQUEST_URI']);
}
?>
<!--
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
-->
<!--Start Header Temaplate-->
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <link rel="stylesheet" href="lib/bootstrap/dist/css/bootstrap.min.css" type="text/css" />
  <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="dist/css/styles.min.css" type="text/css" />
  <link rel="icon" href="assets/icons/favicon.png">
  <title>phpMyFarm</title>
</head>
<body>
  <noscript>

  </noscript>
  <div class="se-pre-con"></div>
  <div>
    <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><img class="logo" alt="Logo" src="assets/icons/logo.png" />&nbsp;phpMyFarm</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
						<li><a href="index.php"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;Main</a></li>
						<li><a href="data.php?data"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Remove Data</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-cogs" aria-hidden="true"></i>&nbsp;Settings&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i></a>
                <ul class="dropdown-menu">
								  <li><a href="settings.php?main"><i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;Main Settings</a></li>
                  <li><a href="settings.php?data"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add Info</a></li>
                  <li><a href="settings.php?user"><i class="fa fa-id-card" aria-hidden="true"></i>&nbsp;Add User</a></li>
                </ul>
              </li>
              <li><a href="login.php?logout"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Sign Out&nbsp;<?php echo $session; ?></a></li>
          </ul>
        </div>
      </div>
    </nav>
      <div id="wrapper">
        <div class="container">
          <div class="page-header">
            <h3><?php echo $name; ?></h3>
          </div>
          <div class="row">
            <div class="col-lg-12">
						<div id="message"></div>
<!--End Header Temaplate-->
