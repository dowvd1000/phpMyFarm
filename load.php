<div class="table-responsive">
<table class="table table-bordered table-condensed table-hover">
	<tr>
		<th>Actions</th>
		<th>Ticket No.</th>
		<th>Date</th>
		<th>Crop</th>
		<th>Farm</th>
		<th>Farmer</th>
		<th>Trucker</th>
		<th>Truck</br>Trailer</th>
		<th>Gross Weight</th>
		<th>Tare Weight</th>
		<th>GRD</th>
		<th>TW</th>
		<th>MST%</th>
		<th>DKG%</th>
		<th>CCFM</th>
		<th>Net Weight</th>
		<th>MTS</th>
		<th>DC</th>
		<th>HC</th>
		<th>Notes</th>
	</tr>
	<?php
	require_once 'config/dbconn.php';
	$query = "SELECT * FROM `tickets` ORDER BY `id` DESC";
	$stmt = $DBcon->prepare($query);
	$stmt->execute();
	while ($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
		extract($row);
	?>
	<tr>
		<td>
			<?php
			if (isset($_GET['data'])) {
			?>
			<a data-action="delete" data-id="<?php echo $id; ?>" href="javascript:void(0)"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
			<?php
			}
			?>
			<a href="javascript:void(0)" onclick="javascript:void window.open('print.php?print=<?php echo $id; ?>','','width=600,height=800,resizable=false,left=0,top=0');return false;"><i class="fa fa-print" aria-hidden="true"></i></a>
			<a href="javascript:void(0)" onclick="javascript:void window.open('edit.php?edit=<?php echo $id; ?>','','width=700,height=1000,resizable=false,left=0,top=0');return false;"><i class="fa fa-pencil" aria-hidden="true"></i></a>
		</td>
		<td>#&nbsp;<?php echo $id; ?></td>
		<td><?php echo $time; ?></td>
		<td><?php echo $crop; ?></td>
		<td><a href="sort.php?sort=<?php echo $farm; ?>"><?php echo $farm; ?></a></td>
		<td><?php echo $farmer; ?></td>
		<td><?php echo $trucker; ?></td>
		<td><?php echo $truck; ?></td>
		<td><?php echo $gross; ?>&nbsp;kg</td>
		<td><?php echo $tare; ?>&nbsp;kg</td>
		<td><?php echo $grade; ?></td>
		<td><?php echo $test; ?></td>
		<td><?php echo $moisture; ?>%</td>
		<td><?php echo $dockage; ?>%</td>
		<td><?php echo $ccfm; ?>%</td>
		<td><?php echo $netkg; ?>&nbsp;kg</td>
		<td><?php echo $netmt; ?>&nbsp;mt</td>
		<td>$<?php echo $drying; ?></td>
		<td>$<?php echo $handeling; ?></td>
		<td><?php echo $notes; ?></td>
	</tr>
	<?php
	}
	?>
</table>
</div>
