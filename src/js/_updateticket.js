
$(document).ready(function() {
  $('[data-action="update_ticket"]').click(function() {
    var pid = $(this).attr('data-id');
    var gross = $("#gross").val();
    var tare = $("#tare").val();
		var moisture = $("#moisture").val();
    var dockage = $("#dockage").val();
    var ccfm = $("#ccfm").val();
		var crop = $("#crop").val();
    var farm = $("#farm").val();
    var farmer = $("#farmer").val();
    var trucker = $("#trucker").val();
    var truck = $("#truck").val();
    var test = $("#test").val();
    var grade = $("#grade").val();

    var dataString = 'pid0=' + pid +
                     '&gross0=' + gross +
                     '&tare0=' + tare +
                     '&moisture0=' + moisture +
                     '&dockage0=' + dockage +
                     '&ccfm0=' + ccfm +
                     '&crop0=' + crop +
                     '&farm0=' + farm +
                     '&farmer0=' + farmer +
                     '&trucker0=' + trucker +
                     '&truck0=' + truck +
                     '&test0=' + test +
                     '&grade0=' + grade;

		if (moisture < 12.9) {
			$("#message").append("<div class='alert alert-warning fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;<strong>Warning:</strong>&nbsp;Moisture Value is "+moisture+" can't be less than 12.9!</div>");
		}
    else if (gross === '' ||
             tare === '' ||
             moisture === '' ||
             dockage === '' ||
             ccfm === '' ||
             crop === '' ||
             farm === '' ||
             farmer === '' ||
             truck === '' ||
             test === '' ||
             grade === ''
    ) {
      $("#message").append("<div class='alert alert-warning fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;<strong>Warning:</strong>&nbsp;Ticket form empty!</div>");
		} else {
			bootbox.dialog({
				message: "Are you sure you want to Update?",
				title: "<i class='fa fa-plus' aria-hidden='true'></i>&nbsp;Update",
				buttons: {
					success: {
						label: "No&nbsp;<i class='fa fa-times' aria-hidden='true'></i>",
						className: "btn-danger",
						callback: function() {
							$('.bootbox').modal('hide');
						}
					},
					danger: {
						label: "Yes&nbsp;<i class='fa fa-floppy-o' aria-hidden='true'></i>",
						className: "btn-success",
						callback: function() {
							$.ajax({
								type: "POST",
								url: "updatetickets.php",
								data: dataString,
								cache: false,
							})
              .done(function(jqXHR, don) {
								$("#message").append("<div class='alert alert-success fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-check' aria-hidden='true'></i>&nbsp;<strong>Success:</strong>&nbsp;Ticket added successfully. >> "+jqXHR.status+" > "+don+"</div>");
								$(".alert").fadeTo(500, 0).slideUp(300, function(){
									$(this).remove();
								});
                window.close();
                window.onunload = refresh;
                function refresh() {
                  window.opener.location.reload();
                }
							})
							.fail(function(jqXHR, err) {
                $("#message").append("<div class='alert alert-danger fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-times' aria-hidden='true'></i>&nbsp;<strong>Error:</strong>&nbsp;Failed to update ticket. >> "+jqXHR.status+" > "+err+"</div>");
							});
						}
					}
				}
			});
    }
  });
  $('[data-action="clear"]').click(function(){
		clearData();
  });
  $('[data-action="load"]').click(function(){
		loadData();
  });
  loadData();
});
