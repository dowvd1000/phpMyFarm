$(document).ready(function() {
  $('[data-action="addMuser"]').click(function() {
    var name = $("#name").val();
    var pass = $("#pass").val();

    var DataString = 'name0=' + name + '&pass0=' + pass;
    if (name === '' || pass === '') {
      $("#message").append("<div class='alert alert-warning fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;<strong>Warning:</strong>&nbsp;User form empty!</div>");
		} else {
			bootbox.dialog({
				message: "Are you sure you want to Add?",
				title: "<i class='fa fa-plus' aria-hidden='true'></i>&nbsp;Add",
				buttons: {
					success: {
						label: "No&nbsp;<i class='fa fa-times' aria-hidden='true'></i>",
						className: "btn-danger",
						callback: function() {
							$('.bootbox').modal('hide');
						}
					},
					danger: {
						label: "Yes&nbsp;<i class='fa fa-floppy-o' aria-hidden='true'></i>",
						className: "btn-success",
						callback: function() {
							$.ajax({
								type: "POST",
								url: "adduser.php",
								data: DataString,
								cache: false,
							})
							.done(function(jqXHR, don) {
								$("#message").append("<div class='alert alert-success fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-check' aria-hidden='true'></i>&nbsp;<strong>Success:</strong>&nbsp;User added successfully. >> "+jqXHR.status+" > "+don+"</div>");
								$(".alert").fadeTo(500, 0).slideUp(300, function(){
									$(this).remove();
								});
                location.reload();
							})
							.fail(function(jqXHR, err) {
                $("#message").append("<div class='alert alert-danger fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-times' aria-hidden='true'></i>&nbsp;<strong>Error:</strong>&nbsp;Failed to save to database. >> "+jqXHR.status+" > "+err+"</div>");
              });
						}
					}
				}
			});
    }
  });
});
