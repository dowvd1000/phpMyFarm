$(document).ready(function() {
  $.getJSON("package.json", function(json) {
    var yourVersion = json.version;
    $('[data-action="version"]').append(yourVersion);
  });
	$.ajax({
    	type: "GET",
    	url: "vers.xml",
    	dataType: "xml",
    	success: function(root) {
     		$(root).find("root").each(function(){
					var newVersion = $(this).find("vers").text();
        	$('[data-action="new"]').append('<a class="text-muted"  href="https://dowvd1000.github.io/vers.xml"&nbsp;(New Version Avaliable&nbsp;'+ newVersion +')</a>');
      	});
    	}
		});
  $('[data-action="about"]').click(function() {
    bootbox.dialog({
      title: '<i class="fa fa-info" aria-hidden="true"></i>&nbsp;About',
      message: '<h3>phpMyFarm</h3><p>Database for managing grain elavator records.</p><a href="/LICENSE">LICENSE</a><p class="text-muted">&copy; 2016 - 2017 Dow VD</p>'
    });
  });
});
