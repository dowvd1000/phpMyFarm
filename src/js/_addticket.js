$(document).ready(function() {
  function loadData(){
    $('#load').load('load.php');
  }
  $('[data-action="load"]').click(function(){
		loadData();
  });
  function clearData() {
    $("#gross").val(null);
    $("#tare").val(null);
		$("#moisture").val('0');
    $("#dockage").val('0');
    $("#ccfm").val('0');
    $("#notes").val(null);
    $("#test").val(null);
    $("#grade").val(null);
  }
  $('[data-action="add"]').click(function() {
    var gross = $("#gross").val();
    var tare = $("#tare").val();
		var moisture = $("#moisture").val();
    var dockage = $("#dockage").val();
    var ccfm = $("#ccfm").val();
		var crop = $("#crop").val();
    var farm = $("#farm").val();
    var farmer = $("#farmer").val();
    var trucker = $("#trucker").val();
    var truck = $("#truck").val();
    var notes = $("#notes").val();
    var test = $("#test").val();
    var grade = $("#grade").val();

    var dataString = 'gross0=' + gross +
                     '&tare0=' + tare +
                     '&moisture0=' + moisture +
                     '&dockage0=' + dockage +
                     '&ccfm0=' + ccfm +
                     '&crop0=' + crop +
                     '&farm0=' + farm +
                     '&farmer0=' + farmer +
                     '&trucker0=' + trucker +
                     '&truck0=' + truck +
                     '&notes0=' + notes +
                     '&test0=' + test +
                     '&grade0=' + grade;

		if (moisture < 12.9) {
			$("#message").append("<div class='alert alert-warning fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;<strong>Warning:</strong>&nbsp;Moisture Value is "+moisture+" can't be less than 12.9!</div>");
		}
    else if (crop === '0' ||
             farm === '0' ||
             farmer === '0' ||
             truck === '0'
    ) {
      $("#message").append("<div class='alert alert-warning fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;<strong>Warning:</strong>&nbsp;Ticket form empty!</div>");
		} else {
			bootbox.dialog({
				message: "Are you sure you want to Add?",
				title: "<i class='fa fa-plus' aria-hidden='true'></i>&nbsp;Add",
				buttons: {
					success: {
						label: "No&nbsp;<i class='fa fa-times' aria-hidden='true'></i>",
						className: "btn-danger",
						callback: function() {
							$('.bootbox').modal('hide');
						}
					},
					danger: {
						label: "Yes&nbsp;<i class='fa fa-floppy-o' aria-hidden='true'></i>",
						className: "btn-success",
						callback: function() {
							$.ajax({
								type: "POST",
								url: "addticket.php",
								data: dataString,
								cache: false,
							})
              .done(function(jqXHR, don) {
								$("#message").append("<div class='alert alert-success fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-check' aria-hidden='true'></i>&nbsp;<strong>Success:</strong>&nbsp;Ticket added successfully. >> "+jqXHR.status+" > "+don+"</div>");
								$(".alert").fadeTo(500, 0).slideUp(300, function(){
									$(this).remove();
								});
                loadData();
                clearData();
							})
							.fail(function(jqXHR, err) {
                $("#message").append("<div class='alert alert-danger fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-times' aria-hidden='true'></i>&nbsp;<strong>Error:</strong>&nbsp;Failed to save to database. >> "+jqXHR.status+" > "+err+"</div>");
							});
						}
					}
				}
			});
    }
  });
  $('[data-action="clear"]').click(function(){
		clearData();
  });
  $('[data-action="load"]').click(function(){
		loadData();
  });
  loadData();
});
