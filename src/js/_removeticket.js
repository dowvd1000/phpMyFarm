$(document).ready(function() {
  $('[data-action="delete"]').click(function(e) {
		e.preventDefault();
		var pid = $(this).attr('data-id');
		var parent = $(this).parent("td").parent("tr");
		bootbox.dialog({
			message: "Are you sure you want to Delete?",
			title: "<i class='fa fa-trash' aria-hidden='true'></i>&nbsp;Delete",
			buttons: {
				success: {
					label: "No&nbsp;<i class='fa fa-times' aria-hidden='true'></i>",
					className: "btn-danger",
					callback: function() {
						$('.bootbox').modal('hide');
					}
				},
				danger: {
					label: "Yes&nbsp;<i class='fa fa-trash' aria-hidden='true'></i>",
					className: "btn-success",
					callback: function() {
						$.ajax({
							type: 'POST',
							url: 'removeticket.php',
							data: 'delete='+pid
						})
            .done(function(response) {
              $("#message").append("<div class='alert alert-success fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-check' aria-hidden='true'></i>&nbsp;<strong>Success:</strong>&nbsp;"+response+"</div>");
              $(".alert").fadeTo(500, 0).slideUp(300, function(){
                $(this).remove();
              });
              parent.fadeOut('slow');
            })
            .fail(function(jqXHR, err) {
              $("#message").append("<div class='alert alert-danger fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-times' aria-hidden='true'></i>&nbsp;<strong>Error:</strong>&nbsp;Failed to remove from database. >> "+jqXHR.status+" > "+err+"</div>");
            });
					}
				}
			}
		});
  });
});
