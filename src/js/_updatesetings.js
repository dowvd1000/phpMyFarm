$(document).ready(function() {
  $('[data-action="updateMain"]').click(function() {
    var name = $("#name").val();
    var address = $("#address").val();
    var phone = $("#phone").val();

    var dataString = 'name0=' + name + '&address0=' + address + '&phone0=' + phone;
    if (name === '' || address === '' || phone === '') {
      $("#message").append("<div class='alert alert-warning fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;<strong>Warning:</strong>&nbsp;Setting form empty!</div>");
		} else {
			bootbox.dialog({
				message: "Update Settings?",
				title: "<i class='fa fa-cog' aria-hidden='true'></i>&nbsp;Settings",
				buttons: {
					success: {
						label: "No&nbsp;<i class='fa fa-times' aria-hidden='true'></i>",
						className: "btn-danger",
						callback: function() {
							$('.bootbox').modal('hide');
						}
					},
					danger: {
						label: "Yes&nbsp;<i class='fa fa-floppy-o' aria-hidden='true'></i>",
						className: "btn-success",
						callback: function() {
							$.ajax({
								type: "POST",
								url: "updatesettings.php",
								data: dataString,
								cache: false,
							})
              .done(function(jqXHR, don) {
                $("#message").append("<div class='alert alert-success fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-check' aria-hidden='true'></i>&nbsp;<strong>Success:</strong>&nbsp;Updated settings. >> "+jqXHR.status+" > "+don+"</div>");
                $(".alert").fadeTo(500, 0).slideUp(300, function(){
                  $(this).remove();
                });
              })
              .fail(function(jqXHR, err) {
                $("#message").append("<div class='alert alert-danger fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-times' aria-hidden='true'></i>&nbsp;<strong>Error:</strong>&nbsp;Failed to save settings to database. >> "+jqXHR.status+" > "+err+"</div>");
              });
						}
					}
				}
			});
    }
  });
});
