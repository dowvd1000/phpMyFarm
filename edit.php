<?php
$name = "Edit Ticket";
include "assets/templates/header.php";
require_once 'config/dbconn.php';

$id = $_GET['edit'];
$id = trim($_GET['edit']);
$id = strip_tags($id);
$id = htmlspecialchars($id);
if (isset($id)) {
	$query = "SELECT * FROM `tickets` WHERE (`id` LIKE '%".$id."%')";
	$stmt = $DBcon->prepare($query);
	$stmt->execute();
	while ($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
		extract($row);
?>
<div id="form">
  <div class="form-group">
    <div class="input-group">
      <span class="input-group-addon">Farm</span>
      <input class="form-control" id="farm" type="text" value="<?php echo $farm; ?>" />
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <span class="input-group-addon">Crop</span>
      <input class="form-control" id="crop" type="text" value="<?php echo $crop; ?>" />
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <span class="input-group-addon">Truck</span>
      <input class="form-control" id="truck" type="text" value="<?php echo $truck; ?>" />
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <span class="input-group-addon">Gross Weight</span>
      <input class="form-control" id="gross" type="number" value="<?php echo $gross; ?>" />
      <span class="input-group-addon">.kg</span>
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <span class="input-group-addon">Tare Weight</span>
      <input class="form-control" id="tare" type="number" value="<?php echo $tare; ?>" />
      <span class="input-group-addon">.kg</span>
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <span class="input-group-addon">Test Weight</span>
      <input class="form-control" id="test" type="number" value="<?php echo $test; ?>" />
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <span class="input-group-addon">Moisture</span>
      <input class="form-control" id="moisture" type="number" value="<?php echo $moisture; ?>" />
      <span class="input-group-addon">%</span>
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <span class="input-group-addon">Dockage</span>
      <input class="form-control" id="dockage" type="number" value="<?php echo $dockage; ?>" />
      <span class="input-group-addon">%</span>
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <span class="input-group-addon">ccfm</span>
      <input class="form-control" id="ccfm" type="number" value="<?php echo $ccfm; ?>" />
      <span class="input-group-addon">%</span>
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <span class="input-group-addon">Grade</span>
      <input class="form-control" id="grade" type="number" value="<?php echo $grade; ?>" />
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <span class="input-group-addon">Farmer</span>
      <input class="form-control" id="farmer" type="text" value="<?php echo $farmer; ?>" />
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <span class="input-group-addon">Trucker</span>
      <input class="form-control" id="trucker" type="text" value="<?php echo $trucker; ?>" />
    </div>
  </div>
  <button type="button" class="btn btn-danger" onclick="window.close();">Cancel&nbsp;<i class="fa fa-times" aria-hidden="true"></i></button>
  <button data-action="update_ticket" data-id="<?php echo $id; ?>" class="btn btn-success" type="button" data-dismiss="modal">Save&nbsp;<i class="fa fa-floppy-o" aria-hidden="true"></i></button>
</div>

<?php
  }
}
include "assets/templates/footer.php";
?>
