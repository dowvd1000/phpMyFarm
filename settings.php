<?php
$name = "Settings";
require_once 'config/dbconn.php';
include "assets/templates/header.php";

if (isset($_GET['main'])) {
	$query = "SELECT * FROM `settings`";
	$stmt = $DBcon->prepare($query);
	$stmt->execute();
	while ($row=$stmt->fetch(PDO::FETCH_ASSOC) ) {
		extract($row);
	}
	?>
	<div id='form'>
	  <div class='form-group'>
	    <div class='input-group'>
	      <span class='input-group-addon'>Company Name</span>
	      <input class='form-control' id='name' type='text' value='<?php echo $name; ?>' />
	    </div>
	  </div>
		<div class='form-group'>
	    <div class='input-group'>
	      <span class='input-group-addon'>Company Address</span>
	      <input class='form-control' id='address' type='text' value='<?php echo $address; ?>' />
	    </div>
	  </div>
		<div class='form-group'>
	    <div class='input-group'>
	      <span class='input-group-addon'>Company Phone</span>
	      <input class='form-control' id='phone' type='text' value='<?php echo $phone; ?>' />
	    </div>
	  </div>
	</div>
	<div class='form-group'></div>
	<button data-action='updateMain' class='btn btn-success' type='button'>Update&nbsp;<i class='fa fa-floppy-o' aria-hidden='true'></i></button>
	<?php
}
if (isset($_GET['data'])) {
	?>
	<div id='form'>
	  <div class='form-group'>
	    <div class='input-group'>
	      <span class='input-group-addon'>Name</span>
	      <input class='form-control' id='name' type='text' />
	    </div>
	  </div>
		<div class="form-group">
			<select class="form-control" id="type">
				<option value="0">Type</option>
				<option value="farm">Farm</option>
				<option value="truck">Truck/Trailer</option>
				<option value="farmer">Farmer</option>
				<option value="trucker">Trucker</option>
			</select>
		</div>
		<button data-action="addUser" class="btn btn-success" type="button">Add&nbsp;<i class="fa fa-plus" aria-hidden="true"></i></button>
		<div class="form-group"></div>
	</div>
	<table class="table table-bordered table-condensed table-hover table-striped">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Type</th>
			<th>Actions</th>
		</tr>
		<?php
		$query = "SELECT * FROM `data` ORDER BY `id` DESC";
		$stmt = $DBcon->prepare($query);
		$stmt->execute();
		while ($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
			extract($row);
		?>
		<tr>
			<td>#&nbsp;<?php echo $id; ?></td>
			<td><?php echo $name; ?></td>
			<th><?php echo $type; ?></th>
			<td>
				<a data-action="deleteUser" data-id="<?php echo $id; ?>" href="javascript:void(0)"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
			</td>
		</tr>
		<?php
		}
		?>
	</table>
	<?php
}
if (isset($_GET['user'])) {
?>
<div id='form'>
	<div class='form-group'>
		<div class='input-group'>
			<span class='input-group-addon'>Name</span>
			<input class='form-control' id='name' type='text' />
		</div>
	</div>
	<div class="form-group">
		<div class='input-group'>
			<span class='input-group-addon'>Password</span>
			<input class='form-control' id='pass' type='password' />
		</div>
	</div>
	<button data-action="addMuser" class="btn btn-success" type="button">Add&nbsp;<i class="fa fa-plus" aria-hidden="true"></i></button>
	<div class="form-group"></div>
</div>
<table class="table table-bordered table-condensed table-hover table-striped">
	<tr>
		<th>ID</th>
		<th>Name</th>
		<th>Actions</th>
	</tr>
	<?php
	$query = "SELECT * FROM `login` ORDER BY `id` DESC";
	$stmt = $DBcon->prepare($query);
	$stmt->execute();
	while ($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
		extract($row);
	?>
	<tr>
		<td>#&nbsp;<?php echo $id; ?></td>
		<td><?php echo $name; ?></td>
		<td>
			<a data-action="deleteMuser" data-id="<?php echo $id; ?>" href="javascript:void(0)"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
		</td>
	</tr>
	<?php
	}
	?>
</table>
<?php
}
include "assets/templates/footer.php";
?>
