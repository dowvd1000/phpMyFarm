<?php
require_once 'config/dbconn.php';
$query = "SELECT * FROM `settings`";
$stmt = $DBcon->prepare($query);
$stmt->execute();
while ($row=$stmt->fetch(PDO::FETCH_ASSOC) ) {
	extract($row);
  $name = $name;
}
include "assets/templates/header.php";
?>
<div class="form-group">
  <div class="input-group">
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add</button>
    <button type="button" class="btn btn-info" data-action="load"><i class="fa fa-refresh"  aria-hidden="true"></i>&nbsp;Reload</button>
  </div>
</div>
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title"><i class='fa fa-plus' aria-hidden='true'></i>&nbsp;Create Ticket</h4>
      </div>
      <div class="modal-body">
        <div id="form">
					<div class="form-group">
						<select class="form-control" id="farm">
							<option value="0" selected>Farm</option>
							<?php
							$query = "SELECT * FROM `data` WHERE `type`='farm'";
							$stmt = $DBcon->prepare($query);
							$stmt->execute();
							while ($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
								extract($row);
							?>
							<option value="<?php echo $name; ?>"><?php echo $name; ?></option>
							<?php
							}
							?>
						</select>
					</div>
					<div class="form-group">
						<select class="form-control" id="crop">
							<option value="0" selected>Select Crop</option>
							<option value="canola">canola</option>
							<option value="corn">corn</option>
							<option value="soybean">soybean</option>
							<option value="wheat">wheat</option>
						</select>
					</div>
					<div class="form-group">
						<select class="form-control" id="truck">
							<option value="0" selected>Truck/Trailer</option>
							<?php
							$query = "SELECT * FROM `data` WHERE `type`='truck'";
							$stmt = $DBcon->prepare($query);
							$stmt->execute();
							while ($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
								extract($row);
							?>
							<option value="<?php echo $name; ?>"><?php echo $name; ?></option>
							<?php
							}
							?>
						</select>
					</div>
        	<div class="form-group">
        		<div class="input-group">
        			<span class="input-group-addon">Gross Weight</span>
        			<input class="form-control" id="gross" type="number" />
							<span class="input-group-addon">.kg</span>
        		</div>
        	</div>
					<div class="form-group">
        		<div class="input-group">
        			<span class="input-group-addon">Tare Weight</span>
        			<input class="form-control" id="tare" type="number" />
							<span class="input-group-addon">.kg</span>
        		</div>
        	</div>
					<div class="form-group">
        		<div class="input-group">
        			<span class="input-group-addon">Test Weight</span>
        			<input class="form-control" id="test" type="number" value="0" />
        		</div>
        	</div>
        	<div class="form-group">
        		<div class="input-group">
        			<span class="input-group-addon">Moisture</span>
        			<input class="form-control" id="moisture" type="number" />
							<span class="input-group-addon">%</span>
        		</div>
        	</div>
					<div class="form-group">
        		<div class="input-group">
        			<span class="input-group-addon">Dockage</span>
        			<input class="form-control" id="dockage" type="number" value="0" />
							<span class="input-group-addon">%</span>
        		</div>
        	</div>
					<div class="form-group">
        		<div class="input-group">
        			<span class="input-group-addon">ccfm</span>
        			<input class="form-control" id="ccfm" type="number" value="0" />
							<span class="input-group-addon">%</span>
        		</div>
        	</div>
					<div class="form-group">
        		<div class="input-group">
        			<span class="input-group-addon">Grade</span>
        			<input class="form-control" id="grade" type="number" value="0" />
        		</div>
        	</div>
					<div class="form-group">
						<select class="form-control" id="farmer">
							<option value="0" selected>Farmer</option>
							<?php
							$query = "SELECT * FROM `data` WHERE `type`='farmer'";
							$stmt = $DBcon->prepare($query);
							$stmt->execute();
							while ($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
								extract($row);
							?>
							<option value="<?php echo $name; ?>"><?php echo $name; ?></option>
							<?php
							}
							?>
						</select>
					</div>
					<div class="form-group">
						<select class="form-control" id="trucker">
							<option value="0" selected>Trucker</option>
							<?php
							$query = "SELECT * FROM `data` WHERE `type`='trucker'";
							$stmt = $DBcon->prepare($query);
							$stmt->execute();
							while ($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
								extract($row);
							?>
							<option value="<?php echo $name; ?>"><?php echo $name; ?></option>
							<?php
							}
							?>
						</select>
					</div>
					<div class="form-group">
 						<textarea class="form-control" rows="5" id="notes" placeholder="Notes"></textarea>
					</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel&nbsp;<i class="fa fa-times" aria-hidden="true"></i></button>
        <button data-action="clear" class="btn btn-warning">Clear&nbsp;<i class="fa fa-refresh" aria-hidden="true"></i></button>
        <button data-action="add" class="btn btn-success" type="button" data-dismiss="modal">Save&nbsp;<i class="fa fa-floppy-o" aria-hidden="true"></i></button>
      </div>
    </div>
  </div>
</div>
<div id="load"><i class="fa fa-spinner fa-pulse fa-fw"></i>&nbsp;Loading...</div>
<?php include "assets/templates/footer.php"; ?>
