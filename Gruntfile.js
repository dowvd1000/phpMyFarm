// Gruntfile.js
module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    //Javascript
    jshint: {
      options: {
        reporter: require('jshint-stylish')
      },
      all: ['Grunfile.js', 'src/js/*.js']
    },
    concat: {
      options: {
        separator: ';'
      },
      build: {
        files: {
          'dist/js/all.js': 'src/js/*.js'
        }
      }
    },
    uglify: {
      options: {
        banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
      },
      build: {
        files: {
          'dist/js/all.min.js': 'dist/js/all.js'
        }
      }
    },

    //CSS
    lesslint: {
      options: {
        failOnError: false
      },
      src: [
        'src/less/*.less'
      ]
    },
    less: {
      options: {
        banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
      },
      build: {
        files: {
          'dist/css/styles.css': 'src/less/*.less'
        }
      }
    },
		csslint: {
			lax: {
				options: {
					import: false
				},
				src: ['src/css/*.css']
			}
		},
    cssmin: {
      options: {
        banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
      },
      build: {
        files: {
          'dist/css/styles.min.css': 'dist/css/styles.css',
          'dist/css/login.min.css': 'src/css/_login.css'
        }
      }
    },

    //Clean dist folders if rebuilding
    clean: {
      release: [
        'dist/css/*.css',
        'dist/js/*.js'
      ]
    },

    //Create file structure for first build
    mkdir: {
      all: {
        options: {
          mode: 0755,
          create: [
            'lib',
            'dist',
            'dist/css',
            'dist/js'
          ]
        }
      }
    },

    //Watch for changes
    watch: {
      js: {
        files: ['src/js/*.js'],
        tasks: ['concat', 'uglify']
      },
      css: {
        files: ['src/less/*.less'],
        tasks: ['less', 'cssmin']
      },
      config: {
        files: ['Gruntfile.js'],
        options: {
          reload: true
        }
      }
    },

  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-csslint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-mkdir');
  grunt.loadNpmTasks('grunt-lesslint');

  //Tasks
  grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'lesslint', 'less', 'cssmin']);
  grunt.registerTask('rebuild', ['clean', 'jshint', 'concat', 'uglify', 'less', 'cssmin']);
  grunt.registerTask('build', ['mkdir', 'jshint', 'concat', 'uglify', 'less', 'cssmin']);
  grunt.registerTask('travis', ['lesslint', 'jshint']);
  grunt.registerTask('css', ['lesslint', 'csslint', 'less', 'cssmin']);
  grunt.registerTask('js', ['jshint', 'concat', 'uglify']);
};
