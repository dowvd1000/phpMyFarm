<?php
require_once 'config/dbconn.php';
if ($_REQUEST['deleteUser']) {
  $pid = $_REQUEST['deleteUser'];
  $query = "DELETE FROM `data` WHERE id=:pid";
  $stmt = $DBcon->prepare($query);
  $stmt->execute(array(':pid'=>$pid));
  if ($stmt) {
    echo "Failed to remove data from database.";
  }
}
?>
